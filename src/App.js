
import Menu from './components/nav/Menu';
import Home from './pages/Home';
import Products from './pages/Products'
import ProductView from './pages/ProductView';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import AdminDashboard from './pages/Admin'
import CreateProduct from './pages/CreateProduct'
import UpdateProduct from './pages/UpdateProduct'

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { useState, useEffect } from 'react';

import { UserProvider } from './UserContext';
import Cart from './pages/Cart';

export default function App() {

  const [user, setUser] = useState({id: null, isAdmin:false})

  const unSetUser = () => {
    localStorage.clear();
  }

  // useEffect(() => {
  //   console.log(user);
  // }, [user])

  useEffect(() => {
      fetch(`${process.env.REACT_APP_URI}/users/profile`,
        {headers: {
          Authorization: `Bearer ${localStorage.getItem('token')} `
        }})
      .then(response => response.json())
      .then(data => {
        setUser({id: data._id, isAdmin: data.isAdmin});

        
      })
  }, [])



  return (
  <UserProvider value={{user, setUser, unSetUser}}>
    <Router>
      <Menu />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/products" element={<Products />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/logout" element={<Logout />} />
        <Route path="/createProduct" element={<CreateProduct />} />
        <Route path="/updateProduct/:id" element={<UpdateProduct />} />
        <Route path="/adminDashboard" element={<AdminDashboard />} />
        <Route path="/cart/:id" element={<Cart />} />
        <Route path="/products/:productId" element={<ProductView />} />
      </Routes>
    </Router>
  </UserProvider>
  );
}


