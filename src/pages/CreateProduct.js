import { useState, useEffect, useContext } from 'react';
import Jumbotron from '../components/cards/Jumbotron'
import { Container, Col, Row } from 'react-bootstrap'

import UserContext from '../UserContext'
import axios from 'axios'
import toast from 'react-hot-toast'


import { useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2'

export default function CreateProduct() {

  const [name, setName] = useState('');
  const [image, setImage] = useState('');
  const [description, setDescription] = useState('')
  const [price, setPrice] = useState(0)
  const [stock, setStock] = useState(0)

  const [isActive, setIsActive]  = useState(false);

  const { user } = useContext(UserContext)

  const navigate = useNavigate();


  useEffect(()=> {
    if(name !== '' && image !== '' && description !== '' && price !== 0 && stock !== 0){
        setIsActive(true);
    }else{
      setIsActive(false);
    }

  }, [name, image, description, price, stock])


  function createProduct (event){
    event.preventDefault()

    console.log(user)

    if(user.isAdmin){
      fetch(`http://localhost:4000/products/create`, {
        method: 'POST',
        headers: {
          'Content-Type' : 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
          name,
          image,
          description,
          price, 
          stock
          
        })
      })
      .then(response => response.json())
      .then(data => {
        console.log(data)

        if(data.hasImage === false) {
                                Swal.fire({
                                    title: "No Image Uploaded",
                                    icon: "error",
                                    text: "Pleas upload an image"
                                })

        }   else{
          Swal.fire({
            title: "Product Created",
            icon: 'success',
            text: 'Posted product!'
          })
           navigate('/');
        }
      })
    }
    
  }



  return (
    <div>
      <Jumbotron title="Create Product" />
        <Container className='mt-4'>
          <Row>
            <Col className='col-md-6 offset-md-3'>

              <form onSubmit={createProduct}>
              <input type='text' className='form-control mb-4 p-2' placeholder="Enter product name" value={name} onChange={(e) => setName(e.target.value)} required />

              <input type='text' className='form-control mb-4 p-2' placeholder="Upload image" value={image} onChange={(e) => setImage(e.target.value)} required />

              <input type='text' className='form-control mb-4 p-2' placeholder="Description" value={description} onChange={(e) => setDescription(e.target.value)} required />

              <input type='text' className='form-control mb-4 p-2' placeholder="Price" value={price} onChange={(e) => setPrice(e.target.value)} required />

              <input type='stock' className='form-control mb-4 p-2' placeholder="Stock" value={stock} onChange={(e) => setStock(e.target.value)} required />

              <button className='btn btn-success' type='submit' disabled={!isActive}>Create Product</button>
              </form>
            </Col>
          </Row>
        </Container>
    </div>
  );
}
