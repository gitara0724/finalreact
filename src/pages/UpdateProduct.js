import { useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import { useParams, useNavigate, useLocation } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

export default function UpdateProduct() {
	const { id } = useParams();
	const [name, setName] = useState('');
	const [price, setPrice] = useState(0)
	const [stock, setStock] = useState(0)
	const [image, setImage] = useState('');
	const [description, setDescription] = useState('')
	const [isActive, setIsActive] = useState(false);
	const navigate = useNavigate()

	const { user } = useContext(UserContext)

	useEffect(() => {
		if (name !== '' && image !== '' && description !== '' && price !== '' && stock !== '') {
			setIsActive(false);
		} else {
			setIsActive(true)
		}
	}, [name, image, description, price, stock])

	useEffect(() => {
		fetch(`${process.env.REACT_APP_URI}/products/${id}`)
			.then(response => response.json())
			.then(data => {
				setName(data.name);
				setImage(data.image);
				setDescription(data.description);
				setPrice(data.price);
				setStock(data.stock);
			})
	}, [])


	function updateProduct(event) {
		event.preventDefault()
		fetch(`${process.env.REACT_APP_URI}/products/update/${id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name,
				image,
				description,
				price,
				stock
			})
		})
			.then(response => {
				if (response) {
					Swal.fire({
						title: "Product successfully updated!",
						icon: 'success',
						text: "Product Updated!"
					})
				} else {
					Swal.fire({
						title: "Product Update Failed!",
						icon: 'error',
						text: 'Please input valid information.'
					})
				} navigate('/adminDashboard')
			})
	}
	return (
		<Container>
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>

					<form onSubmit={updateProduct}>
						<input type='text' className='form-control mb-4 p-2' placeholder="Enter product name" value={name} onChange={(event) => setName(event.target.value)} required />

						<input type='text' className='form-control mb-4 p-2' placeholder="Upload image" value={image} onChange={(event) => setImage(event.target.value)} required />

						<input type='text' className='form-control mb-4 p-2' placeholder="Description" value={description} onChange={(event) => setDescription(event.target.value)} required />

						<input type='text' className='form-control mb-4 p-2' placeholder="Price" value={price} onChange={(event) => setPrice(event.target.value)} required />

						<input type='stock' className='form-control mb-4 p-2' placeholder="Stock" value={stock} onChange={(event) => setStock(event.target.value)} required />

						<button className='btn btn-success' type='submit' >Update Product</button>
					</form>

				</Col>
			</Row>
		</Container>
	)
}